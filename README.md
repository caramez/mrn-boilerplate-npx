
# MRN Boilerplate NPX

## Getting started


Create an MRN sample front-end project using MRN Design System resources.

Use:
```npx @mrn_ds/create-frontend-app [your application name]```

For more instructions on how to customize the sample project, see
https://mrn-ds-doc.parati.design/